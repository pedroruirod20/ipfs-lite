package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicConnection;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Progress;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsClientServerTest {


    private static final String TAG = IpfsServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void server_stress_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        AtomicBoolean closed = new AtomicBoolean(false);
        AtomicBoolean connected = new AtomicBoolean();
        ipfs.setConnectConsumer(connection -> connected.set(true));
        ipfs.setClosedConsumer(connection -> closed.set(true));
        ipfs.setReachabilityConsumer(reachability -> LogUtils.error(TAG, reachability.name()));

        Session session = ipfs.createSession();

        try {
            DUMMY dummy = DUMMY.getInstance(context);
            Session dummySession = dummy.createSession();
            try {

                PeerId host = ipfs.self();
                assertNotNull(host);
                Multiaddr multiaddr = Multiaddr.getLocalHost(ipfs.getPort());

                // 10 MB
                byte[] input = RandomStringUtils.randomAlphabetic(10000000).getBytes();

                Cid cid = ipfs.storeData(input);
                assertNotNull(cid);

                byte[] cmp = ipfs.getData(session, cid, () -> false);
                assertArrayEquals(input, cmp);

                List<Cid> cids = ipfs.getBlocks(cid);
                LogUtils.debug(TAG, "Links " + cids.size());


                QuicConnection conn = dummy.getHost().dial(dummySession, multiaddr,
                        IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                        IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                Objects.requireNonNull(conn);
                dummySession.swarmEnhance(conn);
                assertTrue(dummySession.swarmContains(conn));

                Thread.sleep(1000);
                assertTrue(connected.get()); // server is connected


                assertEquals(ipfs.numServerConnections(), 1);

                PeerInfo info = dummy.getPeerInfo(conn).
                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                assertNotNull(info);
                assertEquals(info.getAgent(), IPFS.AGENT);
                assertNotNull(info.getObserved());


                byte[] output = dummy.getData(dummySession, cid, new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        LogUtils.error(TAG, "" + progress);
                    }

                    @Override
                    public boolean doProgress() {
                        return true;
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
                assertArrayEquals(input, output);

                conn.close();

                Thread.sleep(1000);

                assertTrue(closed.get());

                assertEquals(ipfs.numServerConnections(), 0);
            } finally {
                dummy.getHost().getServer().shutdown();
                dummySession.clear(true);
                dummy.clearDatabase();
            }
        } finally {
            session.clear(true);
        }
    }
}
