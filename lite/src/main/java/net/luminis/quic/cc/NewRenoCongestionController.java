/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.cc;

import net.luminis.quic.Settings;
import net.luminis.quic.packet.PacketInfo;
import net.luminis.quic.packet.QuicPacket;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import threads.lite.LogUtils;

// https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-6
// "QUIC's congestion control is based on TCP NewReno [RFC6582]."
public class NewRenoCongestionController implements CongestionController {

    private static final String TAG = NewRenoCongestionController.class.getSimpleName();
    protected final CongestionControlEventListener eventListener;
    protected final AtomicLong bytesInFlight = new AtomicLong(0);
    protected final AtomicLong congestionWindow = new AtomicLong(Settings.INITIAL_WINDOW_SIZE);

    private final AtomicLong slowStartThreshold = new AtomicLong(Long.MAX_VALUE);
    private final AtomicReference<Instant> congestionRecoveryStartTime =
            new AtomicReference<>(Instant.MIN);

    public NewRenoCongestionController(CongestionControlEventListener eventListener) {
        this.eventListener = eventListener;
    }


    @Override
    public void registerInFlight(QuicPacket sentPacket) {
        if (!sentPacket.isAckOnly()) {
            eventListener.bytesInFlightIncreased(
                    bytesInFlight.addAndGet(sentPacket.getSize()));
        }
    }

    @Override
    public void registerAcked(List<? extends PacketInfo> acknowlegdedPackets) {
        int epsilon = 3;

        long bytesInFlightCwnd = bytesInFlight.get();
        boolean cwndLimited = congestionWindow.get() - bytesInFlightCwnd <= epsilon;

        int bytesInFlightAcked = acknowlegdedPackets.stream()
                .map(PacketInfo::packet)
                .mapToInt(QuicPacket::getSize)
                .sum();

        if (bytesInFlightAcked > 0) {
            eventListener.bytesInFlightDecreased(bytesInFlight.updateAndGet(l -> l - bytesInFlightAcked));
            checkBytesInFlight();
            LogUtils.debug(TAG, "Bytes in flight decreased to " + bytesInFlight.get() +
                    " (" + acknowlegdedPackets.size() + " packets acked)");
        }

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-6.4
        // "QUIC defines the end of recovery as a packet sent after the start of recovery being acknowledged"
        Instant congestionRecoveryStartTimeCmd = congestionRecoveryStartTime.get();
        Stream<QuicPacket> notBeforeRecovery = acknowlegdedPackets.stream()
                .filter(ackedPacket -> ackedPacket.timeSent().isAfter(
                        congestionRecoveryStartTimeCmd))
                .map(PacketInfo::packet);

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-7.8
        // "When bytes in flight is smaller than the congestion window (...), the congestion window is under-utilized.
        //  When this occurs, the congestion window SHOULD NOT be increased in either slow start or congestion avoidance."
        if (cwndLimited) {
            long congestionWindowCwnd = congestionWindow.get();
            notBeforeRecovery.forEach(p -> congestionWindow.updateAndGet(l -> {
                if (l < slowStartThreshold.get()) {
                    // i.e. mode is slow start
                    return l + p.getSize();
                } else {
                    // i.e. mode is congestion avoidance
                    return l + ((long) Settings.K_MAX_DATAGRAM_SIZE * p.getSize() / l);
                }
            }));
            if (congestionWindow.get() != congestionWindowCwnd) {
                LogUtils.debug(TAG, "Cwnd(+): " + congestionWindow.get() + " (" + getMode()
                        + "); inflight: " + bytesInFlight.get());
            }
        }
    }

    @Override
    public void discard(List<? extends PacketInfo> discardedPackets) {
        long discardedBytes = discardedPackets.stream()
                .map(PacketInfo::packet)
                .mapToInt(QuicPacket::getSize)
                .sum();
        eventListener.bytesInFlightDecreased(bytesInFlight.updateAndGet(l -> l - discardedBytes));

        if (discardedBytes > 0) {
            checkBytesInFlight();
            LogUtils.debug(TAG, "Bytes in flight decreased with " + discardedBytes + " to " +
                    bytesInFlight.get() + " (" + discardedPackets.size() + " packets RESET)");
        }
    }

    public void reset() {
        LogUtils.debug(TAG, "Resetting congestion controller.");
        eventListener.bytesInFlightDecreased(bytesInFlight.updateAndGet(l -> 0));
    }

    private void checkBytesInFlight() {
        if (bytesInFlight.get() < 0) {
            LogUtils.error(TAG, "Inconsistency error in congestion controller;" +
                    " attempt to set bytes in-flight below 0");
            eventListener.bytesInFlightDecreased(bytesInFlight.updateAndGet(l -> 0));
        }
    }

    @Override
    public void registerLost(List<? extends PacketInfo> lostPackets) {
        long lostBytes = lostPackets.stream()
                .map(PacketInfo::packet)
                .mapToInt(QuicPacket::getSize)
                .sum();
        eventListener.bytesInFlightDecreased(bytesInFlight.updateAndGet(l -> l - lostBytes));

        if (lostBytes > 0) {
            checkBytesInFlight();
            LogUtils.debug(TAG, "Bytes in flight decreased to " + bytesInFlight.get() +
                    " (" + lostPackets.size() + " packets lost)");
        }

        if (!lostPackets.isEmpty()) {
            PacketInfo largest = lostPackets.stream().max(Comparator.comparing(p ->
                    p.packet().getPacketNumber())).get();
            fireCongestionEvent(largest.timeSent());
        }
    }

    private void fireCongestionEvent(Instant timeSent) {
        if (timeSent.isAfter(congestionRecoveryStartTime.get())) {
            congestionRecoveryStartTime.set(Instant.now());
            slowStartThreshold.set(congestionWindow.updateAndGet(l -> {
                long window = l / Settings.K_LOSS_REDUCTION_FACTOR;
                if (window < Settings.K_MINIMUM_WINDOW) {
                    return Settings.K_MINIMUM_WINDOW;
                }
                return window;
            }));
        }
    }

    public Mode getMode() {
        if (congestionWindow.get() < slowStartThreshold.get()) {
            return Mode.SlowStart;
        } else {
            return Mode.CongestionAvoidance;
        }
    }


    public boolean canSend(int bytes) {
        return bytesInFlight.get() + bytes < congestionWindow.get();
    }

    public long getBytesInFlight() {
        return bytesInFlight.get();
    }

    public long getWindowSize() {
        return congestionWindow.get();
    }

    @Override
    public long remainingCwnd() {
        return congestionWindow.get() - bytesInFlight.get();
    }

    public enum Mode {
        SlowStart,
        CongestionAvoidance
    }

}

