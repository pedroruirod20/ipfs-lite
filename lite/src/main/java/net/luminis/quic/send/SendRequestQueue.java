/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.send;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.frame.PingFrame;
import net.luminis.quic.frame.QuicFrame;

import java.time.Duration;
import java.time.Instant;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;


public class SendRequestQueue {
    private final EncryptionLevel encryptionLevel;

    private final Deque<SendRequest> requestQueue = new ConcurrentLinkedDeque<>();
    private final Deque<List<QuicFrame>> probeQueue = new ConcurrentLinkedDeque<>();
    private final AtomicReference<Instant> nextAckTime = new AtomicReference<>(null);
    private final AtomicBoolean cleared = new AtomicBoolean(false);

    public SendRequestQueue(EncryptionLevel level) {
        encryptionLevel = level;
    }

    public void addRequest(QuicFrame fixedFrame, Consumer<QuicFrame> lostCallback) {
        requestQueue.addLast(new SendRequest(fixedFrame.getFrameLength(),
                actualMaxSize -> fixedFrame, lostCallback));
    }

    public void addAckRequest() {
        nextAckTime.set(Instant.now());
    }

    public void addAckRequest(int delay) {
        Instant requestedAckTime = Instant.now().plusMillis(delay);
        nextAckTime.updateAndGet(instant -> {
            if (instant == null || requestedAckTime.isBefore(instant)) {
                return requestedAckTime;
            }
            return instant;
        });

    }

    public void addProbeRequest(List<QuicFrame> frames) {
        probeQueue.addLast(frames);
    }

    public boolean hasProbe() {
        return !probeQueue.isEmpty();
    }

    public boolean hasProbeWithData() {
        List<QuicFrame> firstProbe = probeQueue.peekFirst();
        return firstProbe != null && !firstProbe.isEmpty();
    }

    public List<QuicFrame> getProbe() {
        List<QuicFrame> probe = probeQueue.pollFirst();
        // Even when client first checks for a probe, this might happen due to race condition with clear().
        // (and don't bother too much about the chance of an unnecessary probe)
        return Objects.requireNonNullElseGet(probe, () -> List.of(new PingFrame()));
    }

    public boolean mustAndWillSendAck() {
        Instant now = Instant.now();
        AtomicBoolean update = new AtomicBoolean(false);
        nextAckTime.updateAndGet(instant -> {
            boolean must = instant != null && (now.isAfter(instant) ||
                    Duration.between(now, instant).toMillis() < 1);
            update.set(must);
            if (must) {
                return null;
            }
            return instant;
        });
        return update.get();
    }

    @Nullable
    public Instant getAck() {
        return nextAckTime.getAndSet(null);
    }

    @Nullable
    public Instant nextDelayedSend() {
        return nextAckTime.get();
    }

    /**
     * @param estimatedSize The minimum size of the frame that the supplier can produce. When the supplier is
     *                      requested to produce a frame of that size, it must return a frame of the size or smaller.
     *                      This leaves room for the caller to handle uncertainty of how large the frame will be,
     *                      for example due to a var-length int value that may be larger at the moment the frame
     */
    public void addRequest(Function<Integer, QuicFrame> frameSupplier, int estimatedSize, Consumer<QuicFrame> lostCallback) {
        requestQueue.addLast(new SendRequest(estimatedSize, frameSupplier, lostCallback));
    }

    public boolean hasRequests() {
        return !requestQueue.isEmpty();
    }

    @Nullable
    public SendRequest next(int maxFrameLength) {
        if (maxFrameLength < 1) {  // Minimum frame size is 1: some frames (e.g. ping) are just a type field.
            // Forget it
            return null;
        }

        for (Iterator<SendRequest> iterator = requestQueue.iterator(); iterator.hasNext(); ) {
            SendRequest next = iterator.next();
            if (next.getEstimatedSize() <= maxFrameLength) {
                iterator.remove();
                return next;
            }
        }
        // Couldn't find one.
        return null;

    }

    public void clear() {
        clear(true);
    }

    public void clear(boolean dropAcks) {
        cleared.set(true);
        requestQueue.clear();
        probeQueue.clear();
        if (dropAcks) {
            nextAckTime.set(null);
        }
    }

    public boolean isEmpty() {
        return isEmpty(false);
    }

    public boolean isEmpty(boolean ignoreAcks) {
        if (ignoreAcks) {
            return requestQueue.isEmpty();
        } else {
            return requestQueue.isEmpty() && nextAckTime.get() == null;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "SendRequestQueue[" + encryptionLevel + "]";
    }

}

