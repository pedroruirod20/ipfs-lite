package threads.lite.autonat;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import autonat.pb.Autonat;
import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.utils.DataHandler;

public class AutoNatService {

    @NonNull
    public static CompletableFuture<Autonat.Message> dial(
            @NonNull QuicConnection conn, @NonNull PeerId peerId, @NonNull Set<Multiaddr> addresses) {

        CompletableFuture<Autonat.Message> response = new CompletableFuture<>();
        Autonat.Message.PeerInfo.Builder peerInfoBuilder = Autonat.Message.PeerInfo.newBuilder();

        peerInfoBuilder.setId(ByteString.copyFrom(peerId.getBytes()));

        if (addresses.isEmpty()) {
            response.completeExceptionally(new RuntimeException("No addresses defined"));
            return response;
        }

        for (Multiaddr addr : addresses) {
            peerInfoBuilder.addAddrs(ByteString.copyFrom(addr.getBytes()));
        }

        Autonat.Message.Dial dial = Autonat.Message.Dial.newBuilder()
                .setPeer(peerInfoBuilder.build()).build();

        Autonat.Message message = Autonat.Message.newBuilder().
                setType(Autonat.Message.MessageType.DIAL).
                setDial(dial).build();


        conn.createStream(new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        response.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.AUTONAT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message)).
                                    thenApply(QuicStream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(QuicStream stream, ByteBuffer data) throws Exception {
                        response.complete(Autonat.Message.parseFrom(data.array()));
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL)));

        return response;

    }
}
