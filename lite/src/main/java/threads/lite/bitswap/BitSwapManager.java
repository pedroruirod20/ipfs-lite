package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Cancellable;
import threads.lite.data.BlockSupplier;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.LiteHost;
import threads.lite.host.Session;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;


public class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Set<Multiaddr> motherfuckers = ConcurrentHashMap.newKeySet();
    @NonNull
    private final ConcurrentHashMap<Multiaddr, QuicConnection> peers = new ConcurrentHashMap<>();
    @NonNull
    private final BitSwapEngine engine;
    private final boolean findProvidersActive;
    @NonNull
    private final Registry registry = new Registry();
    @NonNull
    private final Consumer<BlockSupplier> supplier;

    public BitSwapManager(@NonNull LiteHost host,
                          @NonNull Session session,
                          @NonNull BlockStore blockStore,
                          @NonNull Consumer<BlockSupplier> supplier,
                          boolean findProvidersActive) {
        this.host = host;
        this.session = session;
        this.blockStore = blockStore;
        this.engine = new BitSwapEngine(blockStore);
        this.supplier = supplier;
        this.findProvidersActive = findProvidersActive;

    }

    private void writeMessage(
            @NonNull QuicConnection conn, @NonNull MessageOuterClass.Message msg) {


        conn.createStream(new StreamDataHandler(new TokenData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        BitSwapManager.this.throwable(conn, throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                IPFS.BITSWAP_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.BITSWAP_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(msg))
                                    .thenApply(QuicStream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing yet to do
                    }
                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));

    }

    public void clear() {
        try {
            peers.values().forEach(QuicConnection::close);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            registry.clear();
            peers.clear();
            motherfuckers.clear();
        }
    }

    private CompletableFuture<Void> findProviders(
            @NonNull Cancellable cancellable, @NonNull Cid cid) {

        return session.findProviders(cancellable, (multiaddr) -> {

            if (motherfuckers.contains(multiaddr)) {
                // not possible to connect
                return;
            }
            // todo support circuit address later
            if (multiaddr.isCircuitAddress() || !multiaddr.strictSupportedAddress()) {
                // this will be changed in the future when circuit addresses are fully supported
                // but circuit addresses are very seldom (old style I think)
                LogUtils.error(TAG, "Ignore " + multiaddr);
                return;
            }
            QuicConnection conn = peers.get(multiaddr);
            if (conn != null) {
                if (!conn.isConnected()) {
                    peers.remove(multiaddr);
                } else {
                    // nothing to do here there
                    return;
                }
            }

            if (cancellable.isCancelled()) {
                return;
            }

            // todo in the future direct dial (when returns completable)
            host.connect(session, Set.of(multiaddr), IPFS.CONNECT_TIMEOUT,
                            IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX)
                    .whenComplete((connection, throwable) -> {
                        if (throwable != null) {
                            motherfuckers.add(multiaddr);
                        } else {
                            LogUtils.error(TAG, "New connection " + multiaddr);
                            peers.put(multiaddr, connection);
                        }
                    });

        }, cid);

    }


    public Block runWantHaves(@NonNull Cancellable cancellable, @NonNull Cid cid) throws IOException {

        registry.register(cid);

        AtomicBoolean providerStart = new AtomicBoolean(false);
        try {

            Set<Multiaddr> haves = new HashSet<>();

            while (!blockStore.hasBlock(cid)) {

                if (cancellable.isCancelled()) {
                    throw new IOException("canceled");
                }

                // fill the peers with the swarm connections
                session.getSwarm().forEach(connection -> peers.putIfAbsent(
                        Multiaddr.create(connection.getRemoteAddress()), connection));

                for (Multiaddr multiaddr : peers.keySet()) {
                    QuicConnection conn = peers.get(multiaddr);
                    if (conn != null) {
                        if (!conn.isConnected()) {
                            peers.remove(multiaddr);
                            continue;
                        }
                        if (!haves.contains(multiaddr)) {
                            haves.add(multiaddr);
                            writeMessage(conn, BitSwapMessage.create(
                                    MessageOuterClass.Message.Wantlist.WantType.Have,
                                    Collections.singletonList(cid)));
                        }
                    }
                }

                if (cancellable.isCancelled()) {
                    throw new IOException("canceled");
                }

                if (findProvidersActive) {
                    try {
                        if (!providerStart.getAndSet(true)) {
                            Timer timer = new Timer();

                            int delay = 0;
                            if (!peers.isEmpty()) {
                                delay = IPFS.BITSWAP_REQUEST_DELAY * 2;
                            }

                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    long start = System.currentTimeMillis();
                                    findProviders(cancellable, cid).
                                            whenComplete((unused, throwable) -> {

                                                LogUtils.info(TAG, "Load Provider Finish "
                                                        + cid.String() +
                                                        " onStart [" +
                                                        (System.currentTimeMillis() - start) +
                                                        "]...");

                                                if (!cancellable.isCancelled()) {
                                                    providerStart.set(false);
                                                }
                                            });
                                }
                            }, delay);

                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        } finally {
            registry.unregister(cid);
        }

        return blockStore.getBlock(cid);
    }


    @Nullable
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws IOException {

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);
                LogUtils.info(TAG, "Block Get " + cid.String());

                try {
                    return runWantHaves(() -> cancellable.isCancelled() || done.get(), cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid.String());
        }
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull MessageOuterClass.Message bsm) {

        try {
            BitSwapMessage msg = BitSwapMessage.create(bsm);

            for (Block block : msg.blocks()) {
                Cid cid = block.getCid();
                if (registry.isRegistered(cid)) {
                    LogUtils.info(TAG, "Received Block " + cid.String() +
                            " " + conn.getRemoteAddress());
                    blockStore.putBlock(block);
                    registry.unregister(cid);
                    supplier.accept(new BlockSupplier(cid, conn.getRemoteAddress()));
                }
            }

            for (Cid cid : msg.haves()) {
                if (registry.isRegistered(cid)) {
                    registry.scheduleWants(cid, conn);
                }
            }
            if (IPFS.BITSWAP_SEND_REPLY_ACTIVE) {
                engine.receiveMessage(conn, bsm);
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @Override
    public void throwable(@NonNull QuicConnection quicConnection, @NonNull Throwable throwable) {

        LogUtils.error(TAG, "" + quicConnection.getRemoteAddress() +
                " error " + throwable.getMessage());

        Multiaddr multiaddr = Multiaddr.create(quicConnection.getRemoteAddress());
        motherfuckers.add(multiaddr);
        peers.remove(multiaddr);
        quicConnection.close();

        if (IPFS.BITSWAP_SEND_REPLY_ACTIVE) {
            engine.throwable(quicConnection, throwable);
        }
    }

    private class Registry extends ConcurrentHashMap<Cid, Timer> {
        private final ConcurrentHashMap<Cid, Integer> delays = new ConcurrentHashMap<>();
        private final ConcurrentHashMap<Cid,
                List<InetSocketAddress>> sends = new ConcurrentHashMap<>();


        public void register(@NonNull Cid cid) {
            this.put(cid, new Timer());
            this.delays.put(cid, 0);
            this.sends.put(cid, new ArrayList<>());
        }

        public void unregister(@NonNull Cid cid) {
            Timer timer = this.remove(cid);
            if (timer != null) {
                timer.cancel();
            }
            this.delays.remove(cid);
            this.sends.remove(cid);
        }

        public boolean isRegistered(@NonNull Cid cid) {
            return this.containsKey(cid);
        }

        public void clear() {
            values().forEach(Timer::cancel);
            super.clear();
            delays.clear();
            sends.clear();
        }

        private boolean hasSend(@NonNull Cid cid, @NonNull InetSocketAddress address) {
            List<InetSocketAddress> addresses = sends.get(cid);
            Objects.requireNonNull(addresses);
            if (addresses.contains(address)) {
                return true;
            } else {
                addresses.add(address);
                return false;
            }
        }

        public void scheduleWants(@NonNull Cid cid, @NonNull QuicConnection conn) {

            Timer timer = this.get(cid);
            if (timer != null) {

                if (hasSend(cid, conn.getRemoteAddress())) {
                    return;
                }

                int delay = delays.computeIfAbsent(cid, cid1 -> 0);

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        LogUtils.debug(TAG, "scheduleWants " +
                                cid.String() + " " + conn.getRemoteAddress());
                        writeMessage(conn, BitSwapMessage.create(
                                MessageOuterClass.Message.Wantlist.WantType.Block,
                                Collections.singletonList(cid)));
                    }
                }, delay);

                delays.put(cid, delay + IPFS.BITSWAP_REQUEST_DELAY);
            }

        }
    }
}
