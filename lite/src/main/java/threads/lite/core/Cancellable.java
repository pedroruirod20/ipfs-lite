package threads.lite.core;


public interface Cancellable {
    boolean isCancelled();
}
