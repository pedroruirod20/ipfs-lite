package threads.lite.core;


public interface Progress extends Cancellable {

    void setProgress(int progress);

    boolean doProgress();

}
