package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.protobuf.InvalidProtocolBufferException;

import crypto.pb.Crypto;
import threads.lite.LogUtils;

public interface Key {
    String TAG = Key.class.getSimpleName();


    static PubKey unmarshalPublicKey(byte[] data) throws InvalidProtocolBufferException {

        try {
            Crypto.PublicKey pms = Crypto.PublicKey.parseFrom(data);

            byte[] pubKeyData = pms.getData().toByteArray();

            switch (pms.getType()) {
                case RSA:
                    return Rsa.unmarshalRsaPublicKey(pubKeyData);
                case ECDSA:
                    return Ecdsa.unmarshalEcdsaPublicKey(pubKeyData);
                case Secp256k1:
                    return Secp256k1.unmarshalSecp256k1PublicKey(pubKeyData);
                case Ed25519:
                    return Ed25519.unmarshalEd25519PublicKey(pubKeyData);
                default:
                    throw new RuntimeException("BadKeyTypeException");
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw throwable;
        }
    }

    @NonNull
    byte[] raw();

}


