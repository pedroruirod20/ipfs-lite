package threads.lite.crypto;

import androidx.annotation.NonNull;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.math.ec.FixedPointUtil;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;

import crypto.pb.Crypto;


public class Secp256k1 {

    private static final X9ECParameters CURVE_PARAMS =
            CustomNamedCurves.getByName("secp256k1");
    private static final ECDomainParameters CURVE;

    static {
        FixedPointUtil.precompute(CURVE_PARAMS.getG());
        CURVE = new ECDomainParameters(CURVE_PARAMS.getCurve(),
                CURVE_PARAMS.getG(), CURVE_PARAMS.getN(), CURVE_PARAMS.getH());
    }


    public static PubKey unmarshalSecp256k1PublicKey(byte[] data) {
        return new Secp256k1PublicKey(new ECPublicKeyParameters(
                CURVE.getCurve().decodePoint(data), CURVE));
    }


    public static final class Secp256k1PublicKey extends PubKey {
        private final ECPublicKeyParameters pub;

        public Secp256k1PublicKey(ECPublicKeyParameters pub) {
            super(Crypto.KeyType.Secp256k1);
            this.pub = pub;
        }

        public static byte[] sha256(byte[] data) {
            return (new org.bouncycastle.jcajce.provider.digest.SHA256.Digest()).digest(data);
        }

        @NonNull
        public byte[] raw() {
            return this.pub.getQ().getEncoded(true);
        }

        public boolean verify(byte[] data, byte[] signature) {

            ECDSASigner signer = new ECDSASigner();
            signer.init(false, this.pub);

            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(signature)) {
                try (ASN1InputStream inputStream = new ASN1InputStream(byteArrayInputStream)) {

                    ASN1Primitive asn1Primitive = inputStream.readObject();

                    if (asn1Primitive == null) {
                        throw new NullPointerException("null cannot be cast to non-null type org.bouncycastle.asn1.ASN1Sequence");
                    } else {
                        ASN1Encodable[] asn1Encodables = ((ASN1Sequence) asn1Primitive).toArray();


                        if (asn1Encodables.length != 2) {
                            throw new RuntimeException("Invalid signature: expected 2 values for 'r'" +
                                    " and 's' but got " + asn1Encodables.length);
                        } else {
                            ASN1Primitive primitive = asn1Encodables[0].toASN1Primitive();
                            if (primitive == null) {
                                throw new NullPointerException("null cannot be cast to non-null" +
                                        " type org.bouncycastle.asn1.ASN1Integer");
                            } else {
                                BigInteger r = ((ASN1Integer) primitive).getValue();
                                primitive = asn1Encodables[1].toASN1Primitive();
                                if (primitive == null) {
                                    throw new NullPointerException("null cannot be cast to non-null " +
                                            "type org.bouncycastle.asn1.ASN1Integer");
                                } else {
                                    BigInteger s = ((ASN1Integer) primitive).getValue();
                                    return signer.verifySignature(sha256(data), r.abs(), s.abs());
                                }
                            }
                        }
                    }
                }
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

        public int hashCode() {
            return this.pub.hashCode();
        }
    }

}
