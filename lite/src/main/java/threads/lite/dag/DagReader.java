package threads.lite.dag;


import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import merkledag.pb.Merkledag;
import threads.lite.core.Cancellable;
import threads.lite.format.Stage;
import threads.lite.format.Visitor;
import threads.lite.format.Walker;
import unixfs.pb.Unixfs;

public class DagReader {

    public final AtomicInteger atomicLeft = new AtomicInteger(0);
    private final long size;
    private final Visitor visitor;
    private final Walker dagWalker;


    public DagReader(@NonNull Walker dagWalker, long size) {
        this.dagWalker = dagWalker;
        this.size = size;
        this.visitor = new Visitor(dagWalker.getRoot());

    }

    public static DagReader create(@NonNull Merkledag.PBNode node, @NonNull NodeService nodeService) {
        long size = 0;

        Unixfs.Data unixData = DagReader.getData(node);

        switch (unixData.getType()) {
            case Raw:
            case File:
                size = unixData.getFilesize();
                break;
        }


        Walker dagWalker = Walker.createWalker(node, nodeService);
        return new DagReader(dagWalker, size);

    }

    @Nullable
    public static Merkledag.PBLink getLinkByName(@NonNull Merkledag.PBNode node,
                                                 @NonNull String name) {

        for (Merkledag.PBLink link : node.getLinksList()) {
            if (Objects.equals(link.getName(), name)) {
                return link;
            }
        }
        return null;

    }

    @NonNull
    public static Unixfs.Data getData(@NonNull Merkledag.PBNode node) {
        try {
            return Unixfs.Data.parseFrom(node.getData().toByteArray());
        } catch (Throwable throwable) {
            throw new RuntimeException();
        }
    }

    public long getSize() {
        return size;
    }

    public void seek(@NonNull Cancellable cancellable, long offset) throws IOException {
        Pair<Stack<Stage>, Long> result = dagWalker.seek(cancellable, offset);
        this.atomicLeft.set(result.second.intValue());
        this.visitor.reset(result.first);
    }

    @Nullable
    public ByteString loadNextData(@NonNull Cancellable cancellable) throws IOException {

        int left = atomicLeft.getAndSet(0);
        if (left > 0) {
            Stage stage = visitor.peekStage();
            Merkledag.PBNode node = stage.getNode();

            if (node.getLinksCount() == 0) {
                return readUnixNodeData(stage.getData(), left);
            }
        }

        while (true) {
            Stage stage = dagWalker.next(cancellable, visitor);
            if (stage == null) {
                return null;
            }

            Merkledag.PBNode node = stage.getNode();
            if (node.getLinksCount() > 0) {
                continue;
            }

            Unixfs.Data unixData = stage.getData();
            return readUnixNodeData(unixData, 0);
        }
    }

    private ByteString readUnixNodeData(@NonNull Unixfs.Data unixData, int position) {

        switch (unixData.getType()) {
            case Directory:
            case File:
            case Raw:
                return unixData.getData().substring(position);
            default:
                throw new RuntimeException("found %s node in unexpected place " +
                        unixData.getType().name());
        }
    }
}
