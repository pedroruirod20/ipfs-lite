package threads.lite.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;

@Entity
public class Block {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "cid")
    @TypeConverters(Cid.class)
    private final Cid cid;
    @NonNull
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private final byte[] data;

    Block(@NonNull Cid cid, @NonNull byte[] data) {
        this.cid = cid;
        this.data = data;

    }

    public static Block createBlock(@NonNull Cid cid, @NonNull byte[] data) {
        return new Block(cid, data);
    }


    @NonNull
    public byte[] getData() {
        return data;
    }


    @NonNull
    public Cid getCid() {
        return cid;
    }


}
