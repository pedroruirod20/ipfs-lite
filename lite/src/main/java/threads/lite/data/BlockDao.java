package threads.lite.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;

@Dao
public interface BlockDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBlock(Block block);

    @Query("DELETE FROM Block WHERE cid = :cid")
    @TypeConverters(Cid.class)
    void deleteBlock(Cid cid);

    @Query("SELECT 1 FROM Block WHERE cid = :cid")
    @TypeConverters(Cid.class)
    boolean hasBlock(Cid cid);

    @Query("SELECT * FROM Block WHERE cid = :cid")
    @TypeConverters(Cid.class)
    Block getBlock(Cid cid);

    @Query("SELECT data FROM Block WHERE cid = :cid")
    @TypeConverters(Cid.class)
    byte[] getData(Cid cid);
}
