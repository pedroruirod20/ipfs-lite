package threads.lite.dht;

import androidx.annotation.NonNull;

import com.google.common.collect.Iterables;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.LogUtils;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;
import threads.lite.core.Cancellable;

public class Query {

    private static final String TAG = Query.class.getSimpleName();

    private static List<QueryPeer> transform(@NonNull ID key, @NonNull List<Peer> peers) {
        List<QueryPeer> result = new ArrayList<>();
        peers.forEach(peer -> result.add(QueryPeer.create(peer, key)));
        return result;
    }

    public static CompletableFuture<Void> runQuery(@NonNull KadDht dht,
                                                   @NonNull Cancellable cancellable,
                                                   @NonNull ID key,
                                                   @NonNull List<QueryPeer> seedPeers,
                                                   @NonNull KadDht.QueryFunc queryFn) {

        QueryPeerSet queryPeers = new QueryPeerSet();

        ExecutorService executor =
                Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());


        return iteration(executor, queryPeers, dht, cancellable,
                key, seedPeers, queryFn).whenComplete((unused, throwable) -> {
                    executor.shutdown();
                    executor.shutdownNow();
                }
        );
    }


    public static CompletableFuture<Void> iteration(
            @NonNull ExecutorService executor,
            @NonNull QueryPeerSet queryPeers,
            @NonNull KadDht dht,
            @NonNull Cancellable cancellable,
            @NonNull ID key,
            @NonNull List<QueryPeer> current,
            @NonNull KadDht.QueryFunc queryFn) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        if (cancellable.isCancelled()) {
            done.complete(null);
            return done;
        }

        // the peers in query update are added to the queryPeers
        for (QueryPeer peer : current) {
            if (Objects.equals(peer.getPeer().getPeerId(), dht.self)) { // don't add self.
                continue;
            }
            queryPeers.tryAdd(peer);  // set initial state to PeerHeard
        }

        boolean result = queryPeers.numWaitingOrHeard() == 0;
        if (result) {
            done.complete(null);
            return done;
        }

        List<QueryPeer> nextPeersToQuery = queryPeers.nextHeardPeers();

        // try spawning the queries, if there are no available peers to query then we won't spawn them

        List<CompletableFuture<Void>> subtasks = new ArrayList<>();
        for (QueryPeer queryPeer : nextPeersToQuery) {
            queryPeer.setState(PeerState.PeerWaiting);
            CompletableFuture<Void> subtask = new CompletableFuture<>();
            subtasks.add(subtask);
            if (!executor.isShutdown()) {
                executor.execute(() -> {

                    try {
                        List<Peer> newPeers = queryFn.query(cancellable, queryPeer.getPeer());

                        // query successful, try to add to routing table
                        dht.addToRouting(queryPeer);
                        queryPeer.setState(PeerState.PeerQueried);

                        iteration(executor, queryPeers, dht, cancellable, key,
                                transform(key, newPeers), queryFn).whenComplete(
                                (unused, throwable) -> {
                                    if (throwable != null) {
                                        subtask.completeExceptionally(throwable);
                                    } else {
                                        subtask.complete(unused);
                                    }
                                }
                        );

                    } catch (CancellationException cancelException) {
                        subtask.complete(null);
                    } catch (ConnectException connectException) {
                        dht.removeFromRouting(queryPeer);
                        queryPeer.setState(PeerState.PeerUnreachable);
                        subtask.complete(null);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        dht.removeFromRouting(queryPeer);
                        queryPeer.setState(PeerState.PeerUnreachable);
                        subtask.complete(null);
                    }
                });
            }
        }
        CompletableFuture.allOf(Iterables.toArray(subtasks, CompletableFuture.class))
                .whenComplete((unused, throwable) -> {
                    if (throwable != null) {
                        done.completeExceptionally(throwable);
                    } else {
                        done.complete(unused);
                    }
                });

        return done;

    }

}
