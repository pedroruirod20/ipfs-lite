package threads.lite.dht;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;

public class RoutingTable {

    private static final String TAG = RoutingTable.class.getSimpleName();
    private final ConcurrentSkipListSet<Peer> peers = new ConcurrentSkipListSet<>(
            Comparator.comparingLong(Peer::getLatency));

    public List<QueryPeer> nearestPeers(@NonNull ID key) {

        ArrayList<QueryPeer> pds = new ArrayList<>();

        peers.forEach(peer -> pds.add(QueryPeer.create(peer, key)));

        // Sort by distance to target
        Collections.sort(pds);

        // now get the best result to limit 'IPFS.DHT_ALPHA'
        return pds.stream().limit(IPFS.DHT_ALPHA).collect(Collectors.toList());

    }


    public void addPeer(@NonNull QueryPeer queryPeer) {
        addPeer(queryPeer.getPeer());
    }

    @Nullable
    public Peer weakest() {
        return peers.last();
    }

    public void addPeer(@NonNull Peer peer) {
        try {
            // peer already exists in the Routing Table.
            if (peers.contains(peer)) {
                return;
            }


            // We have enough space in the bucket
            if (peers.size() < IPFS.DHT_TABLE_SIZE) {
                peers.add(peer);
                return;
            }

            Peer weakest = weakest();

            if (weakest != null) {
                // let's evict it and add the new peer
                if (weakest.isReplaceable()) {
                    if (weakest.getLatency() > peer.getLatency()) {

                        if (removePeer(weakest)) {
                            peers.add(peer);
                        }
                    }
                }
            }
        } finally {
            LogUtils.info(TAG, toString());
        }
    }

    public boolean removePeer(QueryPeer peer) {
        return removePeer(peer.getPeer());
    }


    private boolean removePeer(@NonNull Peer p) {
        if (p.isReplaceable()) {
            return peers.remove(p);
        }
        return false;
    }

    public void clear() {
        peers.clear();
    }

    public boolean isEmpty() {
        return peers.isEmpty();
    }

    @NonNull
    @Override
    public String toString() {
        return "RoutingTable{" +
                "peers=" + peers.size() +
                '}';
    }
}