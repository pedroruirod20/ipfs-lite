package threads.lite.holepunch;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import holepunch.pb.Holepunch;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.StreamHandler;
import threads.lite.ident.IdentityService;
import threads.lite.relay.RelayConnection;
import threads.lite.utils.DataHandler;


public class HolePunchService {
    public static final String TAG = HolePunchService.class.getSimpleName();
    private static final int STREAM_TIMEOUT = 30; // original is 60 sec

    @NonNull
    public static QuicConnection directConnect(
            @NonNull Session session, @NonNull Multiaddr relayAddress, @NonNull PeerId peerId,
            int timeout, int maxIdleTimeoutInSeconds, int initialMaxStreams,
            int initialMaxStreamData) throws Exception {

        if (Objects.equals(peerId, session.getHost().self())) {
            throw new RuntimeException("No connection to yourself");
        }

        synchronized (peerId.toBase58().intern()) {

            // spec: https://github.com/libp2p/specs/blob/master/relay/DCUtR.md
            QuicConnection relay = session.getHost().dial(relayAddress, true,
                    IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);


            AtomicReference<Multiaddr> observed = new AtomicReference<>();
            relay.setPeerInitiatedStreamCallback(
                    stream -> new StreamHandler(relay, stream) {

                        @Override
                        public void holePunchConnect(@NonNull List<Multiaddr> multiaddrs, long rtt) {
                            LogUtils.error(TAG, "not expected here");
                        }

                        @Nullable
                        @Override
                        public Multiaddr getObservedAddress() {
                            return observed.get();
                        }

                        @NonNull
                        @Override
                        public Session getSession() {
                            return session;
                        }
                    });


            PeerInfo relayInfo = IdentityService.getPeerInfo(relay).
                    get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

            Multiaddr observedAddress = relayInfo.getObserved();
            Objects.requireNonNull(observedAddress); // todo  nice mechanism
            observed.set(observedAddress);
            LogUtils.error(TAG, "[Client] Observed Address " + observed);

            RelayConnection conn = RelayConnection.createRelayConnection(relay, peerId);

            PeerInfo info = IdentityService.getPeerInfo(conn)
                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

            try {
                LogUtils.error(TAG, info.toString());

                Set<Multiaddr> addresses = new HashSet<>();
                for (Multiaddr multiaddr : info.getAddresses()) {
                    if (!multiaddr.isCircuitAddress()) {
                        addresses.add(multiaddr);
                    }
                }
                if (!addresses.isEmpty()) {
                    return session.getHost().connect(session, addresses, timeout,
                                    maxIdleTimeoutInSeconds, initialMaxStreams,
                                    initialMaxStreamData)
                            .get(timeout, TimeUnit.SECONDS);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return initiateHolePunch(session, conn, observedAddress,
                    timeout, maxIdleTimeoutInSeconds, initialMaxStreams, initialMaxStreamData)
                    .get(STREAM_TIMEOUT, TimeUnit.SECONDS);
        }
    }

    @NonNull
    private static CompletableFuture<QuicConnection> initiateHolePunch(
            @NonNull Session session,
            @NonNull RelayConnection conn,
            @NonNull Multiaddr observed,
            int timeout,
            int maxIdleTimeoutInSeconds,
            int initialMaxStreams,
            int initialMaxStreamData) {

        Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                .setType(Holepunch.HolePunch.Type.CONNECT);
        builder.addObsAddrs(ByteString.copyFrom(observed.getBytes()));

        Holepunch.HolePunch message = builder.build();

        long time = System.currentTimeMillis();

        CompletableFuture<QuicConnection> done = new CompletableFuture<>();
        CompletableFuture<QuicStream> stream = conn.createStream(
                new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.HOLE_PUNCH_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message));
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(QuicStream stream, ByteBuffer data) throws Exception {

                        Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data.array());

                        long rtt = System.currentTimeMillis() - time;
                        LogUtils.error(TAG, "[Client] Request took " + rtt);
                        Objects.requireNonNull(msg);


                        if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
                            stream.closeOutput();
                            throw new Exception("no connection");
                        }

                        Set<Multiaddr> addresses = new HashSet<>();
                        List<ByteString> entries = msg.getObsAddrsList();
                        if (entries.size() == 0) {
                            throw new Exception("no observed addresses");
                        }

                        for (ByteString entry : entries) {
                            try {
                                Multiaddr multiaddr = Multiaddr.create(entry);
                                boolean relayConnection = multiaddr.isCircuitAddress();
                                if (!relayConnection) {
                                    addresses.add(multiaddr);
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }

                        msg = Holepunch.HolePunch.newBuilder().
                                setType(Holepunch.HolePunch.Type.SYNC).build();

                        stream.writeOutput(DataHandler.encode(msg)).
                                thenApply(QuicStream::closeOutput);


                        // wait for sync to reach the other peer and then punch a hole for it in our NAT
                        // by attempting a connect to it.
                        Thread.sleep(rtt / 2); // TODO

                        LogUtils.error(TAG, "[Client] Hole Punch Dial Address " + addresses);

                        connect(session, addresses, timeout, maxIdleTimeoutInSeconds,
                                initialMaxStreams, initialMaxStreamData)
                                .whenComplete((connection, throwable) -> {
                                    if (throwable != null) {
                                        done.completeExceptionally(throwable);
                                    } else {
                                        done.complete(connection);
                                    }
                                });
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        stream.whenComplete((stream1, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream1.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL));
            }
        });

        return done;

    }


    @NonNull
    public static CompletableFuture<QuicConnection> connect(
            @NonNull Session session, @NonNull Set<Multiaddr> multiaddrs,
            int timeout, int maxIdleTimeoutInSeconds,
            int initialMaxStreams, int initialMaxStreamData) {

        CompletableFuture<QuicConnection> done = new CompletableFuture<>();

        if (multiaddrs.isEmpty()) {
            done.completeExceptionally(new ConnectException("no addresses left"));
            return done;
        }

        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        AtomicInteger counter = new AtomicInteger(0);
        for (Multiaddr address : multiaddrs) {
            counter.incrementAndGet();
            executor.execute(() -> {
                try {
                    QuicConnection conn = session.getHost().dial(address, true, timeout,
                            maxIdleTimeoutInSeconds, initialMaxStreams,
                            initialMaxStreamData);
                    session.getHost().setDefaultStreamCallback(session, conn);
                    done.complete(conn);
                    executor.shutdownNow();
                } catch (Throwable throwable) {
                    if (counter.decrementAndGet() == 0) {
                        if (!done.isDone()) {
                            done.completeExceptionally(throwable);
                        }
                    }
                }
            });

        }
        executor.shutdown();
        return done;
    }

}
