package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.minidns.DnsClient;
import org.minidns.cache.LruCache;
import org.minidns.dnsmessage.DnsMessage;
import org.minidns.dnsqueryresult.DnsQueryResult;
import org.minidns.record.Data;
import org.minidns.record.Record;
import org.minidns.record.TXT;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Protocol;
import threads.lite.cid.ProtocolSupport;


public class DnsResolver {
    public static final String DNS_ADDR = "dnsaddr=";
    public static final String DNS_LINK = "dnslink=";
    private static final String IPv4 = "/ip4/";
    private static final String IPv6 = "/ip6/";
    private static final String DNS_ADDR_PATH = "/dnsaddr/";
    private static final String DNS_PATH = "/dns/";
    private static final String DNS4_PATH = "/dns4/";
    private static final String DNS6_PATH = "/dns6/";
    private static final String TAG = DnsResolver.class.getSimpleName();
    private static volatile DnsClient INSTANCE = null;


    @NonNull
    public static String resolveDnsLink(@NonNull String host) {

        List<String> txtRecords = getTxtRecords("_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_LINK)) {
                    return txtRecord.replaceFirst(DNS_LINK, "");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, host + " " + throwable.getClass().getName());
            }
        }
        return "";
    }

    @NonNull
    private static List<String> getTxtRecords(@NonNull String host) {
        List<String> txtRecords = new ArrayList<>();

        try {
            DnsClient client = getInstance();
            DnsQueryResult result = client.query(host, Record.TYPE.TXT);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof TXT) {
                    TXT text = (TXT) payload;
                    txtRecords.add(text.getText());
                } else {
                    LogUtils.warning(TAG, payload.toString());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return txtRecords;
    }

    @Nullable
    public static Multiaddr resolveDns(@NonNull ProtocolSupport protocolSupport,
                                       @NonNull String multiaddress) {
        try {
            if (!multiaddress.startsWith(DNS_PATH)) {
                return null;
            }
            String query = multiaddress.replaceFirst(DNS_PATH, "");
            String host = query.split("/")[0];
            InetAddress address = InetAddress.getByName(host);
            String ip = IPv4;
            if (address instanceof Inet6Address) {
                ip = IPv6;
            }
            String hostAddress = address.getHostAddress();
            Objects.requireNonNull(hostAddress);
            Multiaddr multiaddr = Multiaddr.create(ip.concat(query.replaceFirst(host, hostAddress)));
            if (multiaddr.protocolSupported(protocolSupport, true)) {
                return multiaddr;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddress, throwable);
        }
        return null;
    }

    @Nullable
    public static Multiaddr resolveDns4Address(@NonNull ProtocolSupport protocolSupport,
                                               @NonNull String multiaddress) {
        try {
            if (!multiaddress.startsWith(DNS4_PATH)) {
                return null;
            }
            String query = multiaddress.replaceFirst(DNS4_PATH, "");
            String host = query.split("/")[0];
            InetAddress address = InetAddress.getByName(host);
            String ip = IPv4;
            if (address instanceof Inet6Address) {
                ip = IPv6;
            }
            String hostAddress = address.getHostAddress();
            Objects.requireNonNull(hostAddress);
            Multiaddr multiaddr = Multiaddr.create(ip.concat(query.replaceFirst(host, hostAddress)));
            if (multiaddr.protocolSupported(protocolSupport, true)) {
                return multiaddr;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddress, throwable);
        }
        return null;
    }


    @Nullable
    public static Multiaddr resolveDns6Address(@NonNull ProtocolSupport protocolSupport,
                                               @NonNull String multiaddress) {
        try {
            if (!multiaddress.startsWith(DNS6_PATH)) {
                return null;
            }
            String query = multiaddress.replaceFirst(DNS6_PATH, "");
            String host = query.split("/")[0];
            InetAddress address = InetAddress.getByName(host);
            String ip = IPv4;
            if (address instanceof Inet6Address) {
                ip = IPv6;
            }
            String hostAddress = address.getHostAddress();
            Objects.requireNonNull(hostAddress);
            Multiaddr multiaddr = Multiaddr.create(ip.concat(query.replaceFirst(host, hostAddress)));
            if (multiaddr.protocolSupported(protocolSupport, true)) {
                return multiaddr;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddress, throwable);
        }
        return null;
    }

    @Nullable
    public static Multiaddr resolveDns6(@NonNull ProtocolSupport protocolSupport,
                                        @NonNull Multiaddr multiaddr) {
        return resolveDns6Address(protocolSupport, multiaddr.toString());
    }

    @Nullable
    public static Multiaddr resolveDns4(@NonNull ProtocolSupport protocolSupport,
                                        @NonNull Multiaddr multiaddr) {
        return resolveDns4Address(protocolSupport, multiaddr.toString());
    }

    @Nullable
    public static Multiaddr resolveDns(@NonNull ProtocolSupport protocolSupport,
                                       @NonNull Multiaddr multiaddr) {
        return resolveDns(protocolSupport, multiaddr.toString());
    }

    @NonNull
    public static List<Multiaddr> resolveDnsaddr(@NonNull ProtocolSupport protocolSupport,
                                                 @NonNull Multiaddr multiaddr) {
        List<Multiaddr> multiaddrs = new ArrayList<>();
        if (!multiaddr.isDnsaddr()) {
            return multiaddrs;
        }
        String host = multiaddr.getHost();
        Objects.requireNonNull(host);
        String peerId = multiaddr.getStringComponent(Protocol.P2P).get(0);
        Set<Multiaddr> addresses = resolveDnsaddrHost(protocolSupport, host);

        for (Multiaddr addr : addresses) {
            String cmpPeerId = addr.getStringComponent(Protocol.P2P).get(0);
            if (Objects.equals(cmpPeerId, peerId)) {
                multiaddrs.add(addr);
            }
        }
        return multiaddrs;
    }


    @NonNull
    public static Set<Multiaddr> resolveDnsaddrHost(@NonNull ProtocolSupport protocolSupport,
                                                    @NonNull String host) {
        return resolveDnsAddressInternal(protocolSupport, host, new HashSet<>());
    }

    @NonNull
    public static Set<Multiaddr> resolveDnsAddressInternal(@NonNull ProtocolSupport protocolSupport,
                                                           @NonNull String host, @NonNull Set<String> hosts) {
        Set<Multiaddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        List<String> txtRecords = getTxtRecords("_dnsaddr." + host);
        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    if (testRecordReduced.startsWith(DNS_ADDR_PATH)) {
                        String query = testRecordReduced.replaceFirst(DNS_ADDR_PATH, "");
                        String child = query.split("/")[0];
                        multiAddresses.addAll(resolveDnsAddressInternal(protocolSupport, child, hosts));
                    } else if (testRecordReduced.startsWith(DNS4_PATH)) {
                        Multiaddr multiaddr = resolveDns4Address(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else if (testRecordReduced.startsWith(DNS6_PATH)) {
                        Multiaddr multiaddr = resolveDns6Address(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else if (testRecordReduced.startsWith(DNS_PATH)) {
                        Multiaddr multiaddr = resolveDns(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else {
                        Multiaddr multiaddr = Multiaddr.create(testRecordReduced);
                        if (multiaddr.protocolSupported(protocolSupport, true)) {
                            multiAddresses.add(multiaddr);
                        }

                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, host + " " + throwable.getClass().getSimpleName());
            }
        }
        return multiAddresses;
    }

    @NonNull
    public static DnsClient getInstance() {
        if (INSTANCE == null) {
            synchronized (DnsResolver.class) {
                if (INSTANCE == null) {
                    try {
                        INSTANCE = new DnsClient(new LruCache(128));
                    } catch (Throwable e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return INSTANCE;
    }

}
