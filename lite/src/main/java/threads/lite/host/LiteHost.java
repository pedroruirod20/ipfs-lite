package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnection;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import autonat.pb.Autonat;
import circuit.pb.Circuit;
import circuitv1.pb.Circuitv1;
import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.AutoNatService;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.cid.ProtocolSupport;
import threads.lite.data.BlockSupplier;
import threads.lite.format.BlockStore;
import threads.lite.holepunch.HolePunchService;
import threads.lite.ident.IdentityService;
import threads.lite.push.Push;
import threads.lite.relay.RelayService;
import threads.lite.relay.Reservation;


public class LiteHost {


    @NonNull
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    public final AtomicReference<ProtocolSupport> protocol = new AtomicReference<>(ProtocolSupport.UNKNOWN);
    /* NOT YET REQUIRED
    @NonNull

    @NonNull
    private static final TrustManager tm = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String s) {
            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                    }
                }
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String s) {

            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                        remotes.put(peerId, pubKey);
                    }
                }
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };*/
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final KeyPair keypair;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteHostCertificate selfSignedCertificate;
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    private final int port;
    @NonNull
    private final ServerConnector server;
    @NonNull
    private final Session serverSession;
    @Nullable
    private Consumer<Push> incomingPush;
    @Nullable
    private Consumer<QuicConnection> connectConsumer;
    @Nullable
    private Consumer<Reachability> reachabilityConsumer;


    public LiteHost(@NonNull LiteHostCertificate selfSignedCertificate,
                    @NonNull KeyPair keypair,
                    @NonNull BlockStore blockStore,
                    int port) throws IOException {
        this.selfSignedCertificate = selfSignedCertificate;
        this.keypair = keypair;
        this.blockStore = blockStore;
        this.self = PeerId.fromRsaPublicKey(keypair.getPublic());
        evaluateProtocol();

        DatagramSocket socket = getSocket(port);
        this.port = socket.getLocalPort();
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.IETF_draft_29);
        supportedVersions.add(Version.QUIC_version_1);
        this.serverSession = new Session(new BitSwapEngine(blockStore), this);


        this.server = new ServerConnector(socket,
                new FileInputStream(selfSignedCertificate.certificate()),
                new FileInputStream(selfSignedCertificate.privateKey()),
                supportedVersions, false);
        server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public ApplicationProtocolConnection createConnection(
                    String protocol, QuicConnection quicConnection) {

                try {
                    if (connectConsumer != null) {
                        connectConsumer.accept(quicConnection);
                    }
                } catch (Throwable ignore) {
                }

                try {
                    InetSocketAddress address = quicConnection.getRemoteAddress();

                    if (!Multiaddr.isLocalAddress(address.getAddress())) {
                        if (reachabilityConsumer != null) {
                            reachabilityConsumer.accept(Reachability.GLOBAL);
                        }
                    }
                } catch (Throwable ignore) {
                }

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());


                return new ServerHandler(serverSession, quicConnection);

            }
        });
        server.start();
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable throwable) {
            return getSocket(nextFreePort());
        }
    }

    @NonNull
    public ServerConnector getServer() {
        return server;
    }

    public void setConnectConsumer(@Nullable Consumer<QuicConnection> connectConsumer) {
        this.connectConsumer = connectConsumer;
    }

    public void setClosedConsumer(@Nullable Consumer<QuicConnection> closedConsumer) {
        getServer().setClosedConsumer(closedConsumer);
    }

    public void setReachabilityConsumer(@Nullable Consumer<Reachability> reachabilityConsumer) {
        this.reachabilityConsumer = reachabilityConsumer;
        getServer().setReachabilityConsumer(reachabilityConsumer);
    }

    @NonNull
    public KeyPair getKeypair() {
        return keypair;
    }


    @NonNull
    public Set<Reservation> reservations() {
        return reservations;
    }


    @NonNull
    public Session createSession() {
        return createSession(blockSupplier -> {
        }, false);
    }

    @NonNull
    public Session createSession(@NonNull Consumer<BlockSupplier> supplier,
                                 boolean findProvidersActive) {
        return new Session(blockStore, this, supplier, findProvidersActive);
    }

    public PeerId self() {
        return self;
    }


    @NonNull
    private List<Multiaddr> prepareAddresses(@NonNull Set<Multiaddr> multiaddrs) {
        List<Multiaddr> all = new ArrayList<>();
        for (Multiaddr multiaddr : multiaddrs) {

            if (multiaddr.isDns()) {
                Multiaddr ma = DnsResolver.resolveDns(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns6()) {
                Multiaddr ma = DnsResolver.resolveDns6(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns4()) {
                Multiaddr ma = DnsResolver.resolveDns4(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDnsaddr()) {
                all.addAll(DnsResolver.resolveDnsaddr(protocol.get(), multiaddr));
            } else {
                all.add(multiaddr);
            }
        }
        return all;
    }


    @NonNull
    public Set<Multiaddr> listenAddresses() {
        Set<Multiaddr> set = new HashSet<>();
        try {
            if (port > 0) {
                for (InetAddress inetAddress : localAddresses()) {
                    set.add(Multiaddr.create(new InetSocketAddress(inetAddress, port)));
                }
            }
            for (Reservation reservation : reservations()) {
                try {
                    if (reservation.getConnection().isConnected()) {
                        set.addAll(reservation.getReservationAddresses(protocol.get()));
                    } else {
                        reservations.remove(reservation);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return set;

    }

    @NonNull
    public CompletableFuture<QuicConnection> connect(
            @NonNull Session session, @NonNull Set<Multiaddr> multiaddrs, int timeout,
            int maxIdleTimeoutInSeconds, int initialMaxStreams, int initialMaxStreamData) {

        CompletableFuture<QuicConnection> done = new CompletableFuture<>();
        List<Multiaddr> multiaddr = prepareAddresses(multiaddrs);
        int addresses = multiaddr.size();
        if (addresses == 0) {
            done.completeExceptionally(new ConnectException("no addresses left"));
            return done;
        }

        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        AtomicInteger counter = new AtomicInteger(0);
        for (Multiaddr address : multiaddr) {
            counter.incrementAndGet();
            executor.execute(() -> {
                try {
                    QuicConnection conn = dial(session, address, timeout,
                            maxIdleTimeoutInSeconds, initialMaxStreams,
                            initialMaxStreamData);
                    done.complete(conn);
                    executor.shutdownNow();
                } catch (Throwable throwable) {
                    if (counter.decrementAndGet() == 0) {
                        if (!done.isDone()) {
                            done.completeExceptionally(throwable);
                        }
                    }
                }
            });

        }
        executor.shutdown();
        return done;
    }

    // todo when sdk version min is sdk31, then return CompletableFuture<QuicConnection>
    @NonNull
    public QuicConnection dial(@NonNull Session session, @NonNull Multiaddr address,
                               int timeout, int maxIdleTimeoutInSeconds,
                               int initialMaxStreams, int initialMaxStreamData)
            throws ConnectException, CancellationException {

        if (address.isCircuitAddress()) {
            try {

                // todo cleanup, throw nice exceptions (expected two P2P parts)
                // second entry
                PeerId peerId = PeerId.decodeName(address.getStringComponent(Protocol.P2P).get(1));
                Objects.requireNonNull(peerId);
                Multiaddr relayAddress = address.getCircuitAddress();
                LogUtils.error(TAG, relayAddress.toString());
                // TODO timeout, etc are not really passing to the client
                return HolePunchService.directConnect(session, relayAddress, peerId,
                        timeout, maxIdleTimeoutInSeconds, initialMaxStreams, initialMaxStreamData);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                throw new ConnectException(throwable.getMessage());
            }

        } else {
            QuicConnection conn = dial(address, false, timeout,
                    maxIdleTimeoutInSeconds, initialMaxStreams, initialMaxStreamData);

            if (initialMaxStreams > 0) {
                setDefaultStreamCallback(session, conn);
            }
            return conn;

        }
    }

    public void setDefaultStreamCallback(@NonNull Session session, @NonNull QuicConnection conn) {
        conn.setPeerInitiatedStreamCallback(
                stream -> new StreamHandler(conn, stream) {
                    @Override
                    public void holePunchConnect(@NonNull List<Multiaddr> multiaddrs, long rtt) {
                        LogUtils.error(TAG, "not expected here");
                    }

                    @Nullable
                    @Override
                    public Multiaddr getObservedAddress() {
                        return null;
                    }

                    @NonNull
                    @Override
                    public Session getSession() {
                        return session;
                    }
                });
    }


    @NonNull
    public QuicConnection dial(@NonNull Multiaddr address, boolean serverConnect, int timeout,
                               int maxIdleTimeoutInSeconds, int initialMaxStreams,
                               int initialMaxStreamData)
            throws ConnectException, CancellationException {


        long start = System.currentTimeMillis();
        boolean run = false;

        QuicClientConnection conn = getClientConnection(address.getHost(),
                address.getPort(), serverConnect, maxIdleTimeoutInSeconds,
                initialMaxStreams, initialMaxStreamData);

        try {
            conn.connect(timeout).get(timeout, TimeUnit.SECONDS); // todo future remove get
            run = true;
            return conn;
        } catch (ExecutionException | TimeoutException exception) {
            conn.abortHandshake(); // todo hack sdk31
            throw new ConnectException(exception.getMessage());
        } catch (InterruptedException interruptedException) {
            conn.abortHandshake(); // todo hack sdk31
            throw new CancellationException(interruptedException.getMessage());
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, "Dial " +
                        " Success " + run +
                        " Count Success " + success.get() +
                        " Count Failure " + failure.get() +
                        " ServerConnect " + serverConnect +
                        " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    private QuicClientConnection getClientConnection(
            String host, int port, boolean serverConnect, int maxIdleTimeoutInSeconds,
            int initialMaxStreams, int initialMaxStreamData) throws ConnectException {

        int initialMaxData = initialMaxStreamData;
        if (initialMaxStreams > 0) {
            initialMaxData = Integer.MAX_VALUE;
        }

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .version(Version.IETF_draft_29) // in the future switch to version 1
                .noServerCertificateCheck()
                .clientCertificate(selfSignedCertificate.cert())
                .clientCertificateKey(selfSignedCertificate.key())
                .host(host)
                .port(port)
                .alpn(IPFS.ALPN)
                .transportParams(new TransportParameters(
                        maxIdleTimeoutInSeconds, initialMaxData, initialMaxStreamData,
                        initialMaxStreams, 0));
        if (serverConnect) {
            builder.serverConnector(getServer());
        }
        return builder.build();
    }


    public void push(@NonNull QuicConnection connection, @NonNull byte[] data) {
        try {
            Objects.requireNonNull(connection);
            Objects.requireNonNull(data);
            if (incomingPush != null) {
                incomingPush.accept(new Push(connection, new String(data)));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.incomingPush = incomingPush;
    }

    public int numServerConnections() {
        return server.numConnections();
    }


    @NonNull
    public IdentifyOuterClass.Identify createIdentity(@Nullable InetSocketAddress inetSocketAddress) {

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(
                        Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                                setData(ByteString.copyFrom(
                                        keypair.getPublic().getEncoded())).build().toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);

        Set<Multiaddr> addresses = listenAddresses();
        if (!addresses.isEmpty()) {
            for (Multiaddr addr : addresses) {
                builder.addListenAddrs(ByteString.copyFrom(addr.getBytes()));
            }
        }
        List<String> protocols = getProtocols();
        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        if (inetSocketAddress != null) {
            Multiaddr observed = Multiaddr.create(inetSocketAddress);
            builder.setObservedAddr(ByteString.copyFrom(observed.getBytes()));
        }

        return builder.build();
    }

    private List<String> getProtocols() {
        return Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL,
                IPFS.PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP);
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = localAddresses();
        if (!addresses.isEmpty()) {
            protocol.set(getProtocol(addresses));
        } else {
            protocol.set(ProtocolSupport.IPv4);
        }
    }

    private List<InetAddress> localAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();
        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (!Multiaddr.weakLocalAddress(inetAddress)) {
                        inetAddresses.add(inetAddress);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return inetAddresses;
    }

    @NonNull
    private ProtocolSupport getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return ProtocolSupport.IPv6;
        } else if (ipv4) {
            return ProtocolSupport.IPv4;
        } else if (ipv6) {
            return ProtocolSupport.IPv6;
        } else {
            return ProtocolSupport.UNKNOWN;
        }
    }

    public int getPort() {
        return port;
    }

    @NonNull
    public PeerInfo getIdentity() {

        IdentifyOuterClass.Identify identity = createIdentity(null);

        String agent = identity.getAgentVersion();
        String version = identity.getProtocolVersion();
        Multiaddr observed = null;
        if (identity.hasObservedAddr()) {
            try {
                observed = Multiaddr.create(identity.getObservedAddr());
            } catch (Throwable ignore) {
            }
        }

        List<String> protocols = identity.getProtocolsList();
        List<Multiaddr> addresses = new ArrayList<>();
        for (ByteString entry : identity.getListenAddrsList()) {
            try {
                addresses.add(Multiaddr.create(entry));
            } catch (Throwable ignore) {
            }
        }

        return new PeerInfo(agent, version, addresses, protocols, observed);

    }


    public boolean autonat(long timeout) {
        autonat.lock();
        AtomicInteger success = new AtomicInteger(0);
        try {
            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr multiaddr : bootstrap) {

                    service.execute(() -> {
                        try {

                            QuicConnection conn = dial(multiaddr, true,
                                    IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                                    IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);

                            setDefaultStreamCallback(serverSession, conn);
                            try {
                                PeerInfo peerInfo = IdentityService.getPeerInfo(conn).
                                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                                if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {

                                    Set<Multiaddr> addresses = listenAddresses();
                                    CompletableFuture<Autonat.Message> future =
                                            AutoNatService.dial(conn, self, addresses);
                                    Autonat.Message response =
                                            future.get(timeout, TimeUnit.SECONDS);
                                    if (response.hasDialResponse()) {
                                        Autonat.Message.DialResponse dialResponse =
                                                response.getDialResponse();
                                        if (dialResponse.hasStatusText()) {
                                            LogUtils.error(TAG, "Autonat Dial Text : "
                                                    + dialResponse.getStatusText());
                                        }
                                        if (dialResponse.hasStatus()) {
                                            if (dialResponse.getStatus() ==
                                                    Autonat.Message.ResponseStatus.OK) {
                                                success.incrementAndGet();
                                            } else {
                                                success.decrementAndGet();
                                            }
                                        }
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                conn.close();
                            }
                        } catch (Throwable ignore) {
                        }
                    });
                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }

        } finally {
            autonat.unlock();
        }
        return success.get() > 0; // more success then failure
    }


    public Set<Reservation> reservations(@NonNull Set<Reservation.Version> versions, long timeout) {
        reserve.lock();
        try {
            ConcurrentHashMap<PeerId, Set<Reservation.Version>> relays =
                    new ConcurrentHashMap<>();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }
                try {
                    if (reservation.expireInMinutes() < 2) {
                        try {
                            reservation(reservation.getRelayId(),
                                    reservation.getRelayAddress(), versions);
                        } catch (Throwable throwable) {
                            list.remove(reservation);
                        }
                    } else {
                        Set<Reservation.Version> set = relays.get(reservation.getRelayId());
                        if (set == null) {
                            set = new HashSet<>();
                            set.add(reservation.getVersion());
                            relays.put(reservation.getRelayId(), set);
                        } else {
                            set.add(reservation.getVersion());
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : bootstrap) {
                    service.execute(() -> {
                        try {
                            String name = address.getStringComponent(Protocol.P2P).get(0);
                            Objects.requireNonNull(name);
                            PeerId relayId = PeerId.fromBase58(name);
                            Objects.requireNonNull(relayId);

                            Set<Reservation.Version> set = relays.get(relayId);
                            if (set == null) {
                                reservation(relayId, address, versions);
                            } else {
                                if (!Objects.equals(set, versions)) {
                                    versions.removeAll(set);
                                    if (!versions.isEmpty()) {
                                        reservation(relayId, address, versions);
                                    }
                                }
                                // nothing to do here, reservation is sill valid
                            }

                        } catch (Throwable ignore) {
                        }
                    });

                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    private void reservation(@NonNull PeerId relayId,
                             @NonNull Multiaddr relayAddress,
                             @NonNull Set<Reservation.Version> versions) {
        try {
            QuicConnection conn = dial(relayAddress, true, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD_RESERVATION, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);

            AtomicReference<Multiaddr> observed = new AtomicReference<>(null);
            conn.setPeerInitiatedStreamCallback(
                    stream -> new StreamHandler(conn, stream) {
                        @Nullable
                        @Override
                        public Multiaddr getObservedAddress() {
                            return observed.get();
                        }

                        @NonNull
                        @Override
                        public Session getSession() {
                            return serverSession;
                        }

                        public void holePunchConnect(@NonNull List<Multiaddr> multiaddrs, long rtt) {
                            try {
                                Thread.sleep(rtt);  // TODO
                                LogUtils.error(TAG, "[Server] Observed Address " + observed.get());
                                // Upon receiving the Sync, A immediately dials the address to B.
                                ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
                                for (Multiaddr multiaddr : multiaddrs) {
                                    service.execute(() -> {
                                        try {
                                            getServer().punching(
                                                    multiaddr.getInetAddress(), multiaddr.getPort());
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });
                                }
                                boolean finished = service.awaitTermination(
                                        IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                                if (!finished) {
                                    service.shutdownNow();
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });

            boolean success = false;
            try {
                // check if RELAY protocols HOP is supported
                PeerInfo peerInfo = IdentityService.getPeerInfo(conn).
                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                observed.set(peerInfo.getObserved());

                if (versions.contains(Reservation.Version.V2)) {
                    if (peerInfo.hasProtocol(IPFS.RELAY_PROTOCOL_HOP)) {

                        Circuit.HopMessage msg = RelayService.reserve(conn).get(
                                IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                        Objects.requireNonNull(msg);

                        if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
                            if (msg.getStatus() == Circuit.Status.OK) {
                                Reservation reservation = new Reservation(Reservation.Version.V2,
                                        relayId, conn, relayAddress,
                                        Multiaddr.createCircuitAddress(relayAddress, self),
                                        peerInfo.getObserved(),
                                        msg.getReservation(), msg.getLimit());

                                LogUtils.error(TAG, "ipfs swarm connect " +
                                        reservation.getCircuitAddress());
                                reservations.add(reservation);
                                success = true;
                            }
                        }
                    }
                }

                if (versions.contains(Reservation.Version.V1)) {
                    if (peerInfo.hasProtocol(IPFS.RELAY_PROTOCOL)) {
                        Circuitv1.CircuitRelay msg = RelayService.canHop(conn).get(
                                IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                        Objects.requireNonNull(msg);
                        LogUtils.error(TAG, msg.toString());
                        if (msg.getCode() == Circuitv1.CircuitRelay.Status.SUCCESS) {

                            Reservation reservation = new Reservation(Reservation.Version.V1,
                                    relayId, conn, relayAddress,
                                    Multiaddr.createCircuitAddress(relayAddress, self),
                                    peerInfo.getObserved(),
                                    null, null);
                            LogUtils.error(TAG, "ipfs swarm connect " +
                                    reservation.getCircuitAddress());
                            reservations.add(reservation);
                            success = true;
                        }
                    }
                }

            } finally {
                if (!success) {
                    conn.close(); // todo close whould be reflected in server
                }
            }
        } catch (Throwable ignore) {
        }
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public Set<Multiaddr> getBootstrap() {
        return DnsResolver.resolveDnsaddrHost(protocol.get(), IPFS.LIB2P_DNS);
    }

}


