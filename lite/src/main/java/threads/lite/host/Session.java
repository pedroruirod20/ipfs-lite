package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.bitswap.BitSwap;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.data.BlockSupplier;
import threads.lite.dht.KadDht;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.ipns.Ipns;


public class Session {

    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final ConcurrentHashMap<Multiaddr, QuicConnection> swarm = new ConcurrentHashMap<>();
    private final KadDht routing;

    public Session(@NonNull BlockStore blockstore, @NonNull LiteHost host,
                   @NonNull Consumer<BlockSupplier> supplier, boolean findProvidersActive) {
        this.host = host;
        this.bitSwap = new BitSwapManager(host, this, blockstore,
                supplier, findProvidersActive);
        this.routing = new KadDht(host, new Ipns());
    }

    public Session(@NonNull BitSwap bitSwap, @NonNull LiteHost host) {
        this.host = host;
        this.bitSwap = bitSwap;
        this.routing = new KadDht(host, new Ipns());
    }

    @Nullable
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws IOException {
        return bitSwap.getBlock(cancellable, cid);
    }

    public void clear(boolean clearSwarm) {
        if (clearSwarm) swarm.values().forEach(QuicConnection::close);
        bitSwap.clear();
        routing.clear();
        if (clearSwarm) swarm.clear();
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull MessageOuterClass.Message bsm) {
        bitSwap.receiveMessage(conn, bsm);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }


    public boolean swarmHas(@NonNull Multiaddr address) {
        QuicConnection conn = swarm.get(address);
        if (conn != null) {
            if (!conn.isConnected()) {
                swarmReduce(conn);
                return false;
            }
            return true;
        }
        return false;
    }


    public void swarmReduce(@NonNull QuicConnection connection) {
        swarm.remove(Multiaddr.create(connection.getRemoteAddress()));
    }

    public boolean swarmEnhance(@NonNull QuicConnection connection) {
        Multiaddr multiaddr = Multiaddr.create(connection.getRemoteAddress());
        if (!swarmHas(multiaddr)) {
            swarm.put(multiaddr, connection);
            return true;
        }
        return false;
    }

    @NonNull
    public List<QuicConnection> getSwarm() {
        List<QuicConnection> result = new ArrayList<>();
        for (QuicConnection conn : swarm.values()) {
            if (conn.isConnected()) {
                result.add(conn);
            } else {
                swarm.remove(Multiaddr.create(conn.getRemoteAddress()));
            }
        }
        return result;
    }


    public boolean swarmContains(@NonNull QuicConnection connection) {
        return swarm.contains(connection);
    }

    public CompletableFuture<Void> putValue(@NonNull Cancellable cancellable,
                                            @NonNull byte[] key,
                                            @NonNull byte[] data) {
        return routing.putValue(cancellable, key, data);
    }

    public CompletableFuture<Void> findPeer(@NonNull Cancellable cancellable,
                                            @NonNull Consumer<Multiaddr> consumer,
                                            @NonNull PeerId peerID) {
        return routing.findPeer(cancellable, consumer, peerID);
    }

    public CompletableFuture<Void> searchValue(@NonNull Cancellable cancellable,
                                               @NonNull Consumer<Ipns.Entry> consumer,
                                               @NonNull byte[] key) {
        return routing.searchValue(cancellable, consumer, key);
    }


    public CompletableFuture<Void> findProviders(@NonNull Cancellable cancellable,
                                                 @NonNull Consumer<Multiaddr> consumer,
                                                 @NonNull Cid cid) {
        return routing.findProviders(cancellable, consumer, cid);
    }

    public CompletableFuture<Void> provide(@NonNull Cancellable cancellable, @NonNull Cid cid) {
        return routing.provide(cancellable, cid);
    }

    public void throwable(@NonNull QuicConnection quicConnection,
                          @NonNull Throwable throwable) {
        bitSwap.throwable(quicConnection, throwable);
    }
}

