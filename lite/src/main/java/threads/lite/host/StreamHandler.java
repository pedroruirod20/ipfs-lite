package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import bitswap.pb.MessageOuterClass;
import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.utils.DataHandler;


public abstract class StreamHandler {
    private static final String TAG = StreamHandler.class.getSimpleName();
    private final AtomicReference<String> tokenProtocol =
            new AtomicReference<>(null);
    private final AtomicReference<List<Multiaddr>> punchHoleAddrs =
            new AtomicReference<>(Collections.emptyList());
    private final AtomicLong punchHoleStart = new AtomicLong(0L);


    public StreamHandler(@NonNull QuicConnection quicConnection,
                         @NonNull QuicStream stream) {
        stream.setStreamDataConsumer(new StreamDataHandler(new StreamData() {

            @Override
            public void throwable(Throwable throwable) {
                getSession().throwable(quicConnection, throwable);
            }

            @Override
            public void token(QuicStream stream, String token) {
                tokenProtocol.set(token);
                switch (token) {
                    /* NOT YET SUPPORTED
                    case IPFS.RELAY_PROTOCOL_HOP:
                        stream.writeOutput(DataHandler.writeToken(IPFS.RELAY_PROTOCOL_HOP));
                        break;*/
                    /* NOT YET SUPPORTED
                    case IPFS.PING_PROTOCOL:
                        outputStream.write(DataHandler.writeToken(IPFS.PING_PROTOCOL));
                        outputStream.close();
                        break;*/
                    case IPFS.HOLE_PUNCH_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.HOLE_PUNCH_PROTOCOL));
                        break;
                    case IPFS.RELAY_PROTOCOL_STOP:
                        stream.writeOutput(DataHandler.writeToken(IPFS.RELAY_PROTOCOL_STOP));
                        break;
                    case IPFS.STREAM_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.STREAM_PROTOCOL));
                        break;
                    case IPFS.PUSH_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.PUSH_PROTOCOL))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.BITSWAP_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.BITSWAP_PROTOCOL))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.IDENTITY_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.IDENTITY_PROTOCOL));

                        IdentifyOuterClass.Identify response =
                                getSession().getHost().createIdentity(
                                        quicConnection.getRemoteAddress());
                        stream.writeOutput(DataHandler.encode(response))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.TLS_PROTOCOL:
                    case IPFS.SIM_PROTOCOL:
                    case IPFS.NOISE_PROTOCOL:
                        LogUtils.error(TAG, "Ignore " + token +
                                " StreamId " + stream.getStreamId());
                        stream.writeOutput(DataHandler.writeToken(IPFS.NA));
                        break;
                    case IPFS.NA:
                        LogUtils.error(TAG, "Ignore " + token +
                                " StreamId " + stream.getStreamId());
                        stream.closeOutput();
                        break;
                    default:
                        LogUtils.error(TAG, "Ignore " + token +
                                " StreamId " + stream.getStreamId());
                        stream.writeOutput(DataHandler.writeToken(IPFS.NA))
                                .thenApply(QuicStream::closeOutput);
                }

            }

            @Override
            public void fin() {
                // just received fin on input stream
            }

            @Override
            public void data(QuicStream stream, ByteBuffer data) throws Exception {
                String protocol = tokenProtocol.get();
                if (protocol != null) {
                    switch (protocol) {
                        case IPFS.BITSWAP_PROTOCOL: {
                            getSession().receiveMessage(quicConnection,
                                    MessageOuterClass.Message.parseFrom(data.array()));
                            break;
                        }
                        case IPFS.PUSH_PROTOCOL: {
                            getSession().getHost().push(quicConnection, data.array());
                            break;
                        }
                        case IPFS.HOLE_PUNCH_PROTOCOL: {
                            Holepunch.HolePunch holePunch =
                                    Holepunch.HolePunch.parseFrom(data.array());
                            Objects.requireNonNull(holePunch);

                            if (holePunch.getType() == Holepunch.HolePunch.Type.SYNC) {
                                long start = punchHoleStart.getAndSet(0L);
                                List<Multiaddr> obsDial =
                                        punchHoleAddrs.getAndSet(Collections.emptyList());
                                if (start > 0 && !obsDial.isEmpty()) {
                                    long rtt = System.currentTimeMillis() - start;
                                    LogUtils.error(TAG, "[Server] Incoming Sync Punch Hole " + rtt);
                                    holePunchConnect(obsDial, rtt);

                                }
                                stream.closeOutput();
                                break;
                            } else if (holePunch.getType() == Holepunch.HolePunch.Type.CONNECT) {

                                List<ByteString> entries = holePunch.getObsAddrsList();
                                if (entries.size() == 0) {
                                    stream.closeOutput();
                                    break;
                                }

                                List<Multiaddr> addresses = new ArrayList<>();
                                for (ByteString entry : entries) {
                                    try {
                                        Multiaddr multiaddr = Multiaddr.create(entry);
                                        boolean relayConnection = multiaddr.isCircuitAddress();
                                        if (!relayConnection) {
                                            addresses.add(multiaddr);
                                        }
                                    } catch (Throwable throwable) {
                                        LogUtils.error(TAG, throwable);
                                    }
                                }

                                Multiaddr observed = getObservedAddress();
                                Objects.requireNonNull(observed);
                                Holepunch.HolePunch.Builder builder =
                                        Holepunch.HolePunch.newBuilder()
                                                .setType(Holepunch.HolePunch.Type.CONNECT)
                                                .addObsAddrs(ByteString.copyFrom(observed.getBytes()));

                                punchHoleAddrs.set(addresses);
                                punchHoleStart.set(System.currentTimeMillis());

                                stream.writeOutput(DataHandler.encode(builder.build()));
                                break;
                            } else {
                                stream.closeOutput();
                                break;
                            }
                        }
                        /* NOT YET SUPPORTED
                        case IPFS.RELAY_PROTOCOL_HOP: {
                            Circuit.HopMessage hopMessage = Circuit.HopMessage.parseFrom(data.array());
                            Objects.requireNonNull(hopMessage);

                            stream.writeOutput(DataHandler.encode(Circuit.HopMessage.newBuilder()
                                    .setType(Circuit.HopMessage.Type.STATUS)
                                    .setStatus(Circuit.Status.PERMISSION_DENIED)
                                    .build()));

                            break;
                        }*/
                        case IPFS.RELAY_PROTOCOL_STOP: {
                            LogUtils.error(TAG, "RELAY_PROTOCOL_STOP");
                            Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(
                                    data.array());
                            Objects.requireNonNull(stopMessage);

                            if (stopMessage.hasPeer()) {
                                Circuit.StopMessage.Builder builder =
                                        Circuit.StopMessage.newBuilder()
                                                .setType(Circuit.StopMessage.Type.STATUS);
                                builder.setStatus(Circuit.Status.OK);
                                stream.writeOutput(DataHandler.encode(builder.build()));
                            } else {
                                Circuit.StopMessage.Builder builder =
                                        Circuit.StopMessage.newBuilder()
                                                .setType(Circuit.StopMessage.Type.STATUS);
                                builder.setStatus(Circuit.Status.CONNECTION_FAILED);
                                stream.writeOutput(DataHandler.encode(builder.build()));
                            }
                            break;
                        }
                        default:
                            throw new Exception("StreamHandler invalid protocol");
                    }
                } else {
                    throw new Exception("StreamHandler invalid protocol");
                }
            }
        }));
    }


    public abstract void holePunchConnect(@NonNull List<Multiaddr> multiaddrs, long rtt);

    @Nullable
    public abstract Multiaddr getObservedAddress();

    @NonNull
    public abstract Session getSession();
}
