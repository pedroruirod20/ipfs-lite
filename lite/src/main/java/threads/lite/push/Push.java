package threads.lite.push;

import net.luminis.quic.QuicConnection;

public class Push {
    private final QuicConnection connection;
    private final String data;

    public Push(QuicConnection connection, String data) {
        this.connection = connection;
        this.data = data;
    }

    public QuicConnection getConnection() {
        return connection;
    }

    public String getData() {
        return data;
    }
}
