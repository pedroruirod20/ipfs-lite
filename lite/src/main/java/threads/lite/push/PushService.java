package threads.lite.push;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;

public class PushService {

    public static CompletableFuture<Void> notify(@NonNull QuicConnection conn, @NonNull String content) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        conn.createStream(new StreamDataHandler(new TokenData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.PUSH_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(content.getBytes()))
                                    .thenApply(QuicStream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        done.complete(null);
                    }
                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL)));
        return done;
    }
}
