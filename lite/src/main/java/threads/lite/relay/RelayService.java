package threads.lite.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import circuitv1.pb.Circuitv1;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.host.StreamData;
import threads.lite.host.StreamDataHandler;
import threads.lite.utils.DataHandler;

public class RelayService {

    public static final String TAG = RelayService.class.getSimpleName();


    @NonNull
    public static QuicStream getStream(@NonNull QuicConnection conn,
                                       @NonNull PeerId peerId,
                                       @NonNull PeerId self,
                                       long timeout, @NonNull TimeUnit timeoutUnit) throws Exception {

        Circuitv1.CircuitRelay.Peer src = Circuitv1.CircuitRelay.Peer.newBuilder()
                .setId(ByteString.copyFrom(self.getBytes())).build();
        Circuitv1.CircuitRelay.Peer dest = Circuitv1.CircuitRelay.Peer.newBuilder()
                .setId(ByteString.copyFrom(peerId.getBytes())).build();

        Circuitv1.CircuitRelay message = Circuitv1.CircuitRelay.newBuilder()
                .setType(Circuitv1.CircuitRelay.Type.HOP)
                .setSrcPeer(src)
                .setDstPeer(dest)
                .build();

        CompletableFuture<Circuitv1.CircuitRelay> request = new CompletableFuture<>();

        CompletableFuture<QuicStream> result = conn.createStream(
                new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        request.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.RELAY_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message)).
                                    thenApply(QuicStream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here, just comes after data
                    }

                    @Override
                    public void data(QuicStream stream, ByteBuffer data) throws Exception {
                        request.complete(Circuitv1.CircuitRelay.parseFrom(data.array()));
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        result.thenApply(quicStream -> quicStream.writeOutput(
                DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL)));

        Circuitv1.CircuitRelay msg = request.get(timeout, timeoutUnit);
        Objects.requireNonNull(msg);

        if (msg.getType() != Circuitv1.CircuitRelay.Type.STATUS) {
            throw new Exception(msg.getType().name());
        }

        if (msg.getCode() != Circuitv1.CircuitRelay.Status.SUCCESS) {
            throw new Exception(msg.getCode().name());
        }

        return result.get();
    }

    @NonNull
    public static CompletableFuture<Circuit.HopMessage> reserve(@NonNull QuicConnection conn) {
        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.RESERVE).build();


        CompletableFuture<Circuit.HopMessage> reserve = new CompletableFuture<>();
        conn.createStream(new StreamDataHandler(new StreamData() {
            @Override
            public void throwable(Throwable throwable) {
                LogUtils.error(TAG, throwable);
                reserve.completeExceptionally(throwable);
            }

            @Override
            public void token(QuicStream stream, String token) throws Exception {
                if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }
                if (Objects.equals(token, IPFS.RELAY_PROTOCOL_HOP)) {
                    stream.writeOutput(DataHandler.encode(message)).
                            thenApply(QuicStream::closeOutput);
                }
            }

            @Override
            public void fin() {
                // nothing to do here, just comes after data
            }

            @Override
            public void data(QuicStream stream, ByteBuffer data) throws Exception {
                reserve.complete(Circuit.HopMessage.parseFrom(data.array()));
            }

        }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS).thenApply(
                quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP)));

        return reserve;
    }

    @NonNull
    public static QuicStream getStream(@NonNull QuicConnection conn, @NonNull PeerId peerId,
                                       long timeout, @NonNull TimeUnit timeoutUnit)
            throws Exception {
        Circuit.Peer dest = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(peerId.getBytes()))
                .build();

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.CONNECT)
                .setPeer(dest)
                .build();

        CompletableFuture<Circuit.HopMessage> response = new CompletableFuture<>();
        CompletableFuture<QuicStream> result = conn.createStream(
                new StreamDataHandler(new StreamData() {
                    @Override
                    public void throwable(Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        response.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(QuicStream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.RELAY_PROTOCOL_HOP)) {
                            stream.writeOutput(DataHandler.encode(message));
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(QuicStream stream, ByteBuffer data) throws Exception {
                        response.complete(Circuit.HopMessage.parseFrom(data.array()));
                    }

                }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        result.thenApply(quicStream -> quicStream.writeOutput(
                DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP)));


        Circuit.HopMessage msg = response.get(timeout, timeoutUnit);
        Objects.requireNonNull(msg);


        if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
            throw new Exception(msg.getType().name());
        }

        if (msg.getStatus() != Circuit.Status.OK) {
            throw new Exception(msg.getStatus().name());
        }

        if (msg.hasLimit()) {
            Circuit.Limit limit = msg.getLimit();
            if (limit.hasData()) {
                LogUtils.debug(TAG, "Relay Limit Data " + limit.getData());
            }
            if (limit.hasDuration()) {
                LogUtils.debug(TAG, "Relay Limit Duration " +
                        limit.getDuration());
            }
        }
        return result.get();

    }

    public static CompletableFuture<Circuitv1.CircuitRelay> canHop(@NonNull QuicConnection conn) {


        Circuitv1.CircuitRelay message = Circuitv1.CircuitRelay.newBuilder()
                .setType(Circuitv1.CircuitRelay.Type.CAN_HOP)
                .build();


        CompletableFuture<Circuitv1.CircuitRelay> request = new CompletableFuture<>();

        conn.createStream(new StreamDataHandler(new StreamData() {
            @Override
            public void throwable(Throwable throwable) {
                LogUtils.error(TAG, throwable);
                request.completeExceptionally(throwable);
            }

            @Override
            public void token(QuicStream stream, String token) throws Exception {
                if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }
                if (Objects.equals(token, IPFS.RELAY_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(message)).
                            thenApply(QuicStream::closeOutput);
                }
            }

            @Override
            public void fin() {
                // nothing to do here, just comes after data
            }

            @Override
            public void data(QuicStream stream, ByteBuffer data) throws Exception {
                request.complete(Circuitv1.CircuitRelay.parseFrom(data.array()));
            }

        }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS).thenApply(
                quicStream -> quicStream.writeOutput(
                        DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL)));

        return request;


    }

}
